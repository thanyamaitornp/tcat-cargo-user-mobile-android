*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.


Library     AppiumLibrary
Resource         resource.robot

*** Test Cases ***
Open Test Application
    Open Application    remote_url=http://localhost:4723/wd/hub     
    ...     deviceName=TestDevice   
    ...     platformVersion=11.0    
    ...     platformName=Android
    ...     appPackage=com.tcatstudio.tcatcargo
    ...     appActivity=com.tcatstudio.tcatcargo.MainActivity



    
Valid login
    TCAT-01-01 Login User

# Chat
#     TCAT-02-01 Chat

# Tracking 
    # TCAT-03-01 Tracking Display
    # TCAT-03-02 Tracking Add
    # TCAT-03-03 Tracking Edit 
    # TCAT-03-04 Tracking Edit Remark
    # TCAT-03-05 Tracking Delete

# Bill
    # TCAT-04-01 Bill Display
    # TCAT-04-02 Bill Add
    # TCAT-04-03 Bill Cancel
    # TCAT-04-04 Bill Payment
    # TCAT-04-05 Bill Edit Remark

# Topup 
    # TCAT-05-01 Topup Display
    # TCAT-05-02 Topup Add

# Withdraw
    # TCAT-06-01 Withdraw Display
    # TCAT-06-02 Withdraw Add


# Statement
#     TCAT-07-01 Statement Display


# Home-Address
    # TCAT-08-01 Address Add
    # TCAT-08-02 Address Edit

# NoCode
#     TCAT-09-01 NoCode Search

# Home-Contact
#     TCAT-10-01 Contact Display

# News and Promotion
#     TCAT-11-01 News and Promotion Display

# Account
#     TCAT-12-01 Account Edit

# Account-Address
    # TCAT-13-01 Address Add
    # TCAT-13-02 Address Edit

# Bank
    # TCAT-14-01 Bank Add
    # TCAT-14-02 Bank Edit

# PointOrder
    # TCAT-15-01 PointOrder Display
    # TCAT-15-02 PointOrder Add
    # TCAT-15-03 PointOrder Edit Remark
    # TCAT-15-04 PointOrder Cancel
    # TCAT-15-05 PointOrder Payment

# Bill Coupon
    # TCAT-22-01 Coupon Add
    # TCAT-22-02 Coupon Edit Remark
    # TCAT-22-03 Coupon Payment
    # TCAT-22-04 Coupon Delete

Coupon free
    TCAT-23-01 Coupon free Display
    
# Coupon
#     TCAT-16-01 Coupon Display

# Change Password
#     TCAT-17-01 Change Password 

# Account-Contact
#     TCAT-18-01 Contact Display

# Info
#     TCAT-19-01 Info Display

# Terms of Service 
#     TCAT-20-01 Terms of Service 

# Log out
#     TCAT-21-01 Log out

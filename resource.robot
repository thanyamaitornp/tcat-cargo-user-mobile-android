*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.

Library     AppiumLibrary



*** Variables ***

${Name_Tracking}    MTESTROBOT03
${Bill_Tracking}    ATEST-MOBILE01
${Screenshot}       TCAT
${folder}           /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-android
${namecouponfree}           name3

*** Keywords ***

TCAT-01-01 Login User
    Sleep       6s
    Wait until element is visible           accessibility_id=email
    Input Text             accessibility_id=email       jkrit.sr@gmail.com
    Wait until element is visible           accessibility_id=password
    Input Text             accessibility_id=password       123456
    Capture Page Screenshot          ${folder}/Capture/Capture-TCAT01/${Screenshot}01-01.png
    Sleep   2s
    Tap          accessibility_id=submit
    Sleep   5s
    Capture Page Screenshot          ${folder}/Capture/Capture-TCAT01/${Screenshot}01-02.png
    Sleep   3s

TCAT-02-01 Chat
    Sleep       2s
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.ChatBox
    Tap          accessibility_id=grid.ChatBox
    Capture Page Screenshot          ${folder}/Capture/Capture-TCAT02/${Screenshot}02-01.png
    Wait until element is visible           accessibility_id=message
    Input Text          accessibility_id=message          สวัสดีค่ะ
    # Wait until element is visible           accessibility_id=file.gallery
    # Tap           accessibility_id=file.gallery
    # Sleep       3s
    # Tap           id=com.android.documentsui:id/app_icon
    # Sleep       3s
    # Tap           accessibility_id=Show roots
    # Sleep      2s
    # Tap         xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.FrameLayout
    # Sleep      2s
    # Tap         xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[4]/com.google.android.material.card.MaterialCardView/android.widget.FrameLayout/android.widget.LinearLayout
    # Sleep      2s
    # Tap         xpath=//android.widget.LinearLayout[@content-desc="Kbank.png, 42.46 kB, Nov 4"]/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.ImageView
    # Sleep      2s
    Capture Page Screenshot          ${folder}/Capture-TCAT02/${Screenshot}02-02.png

    Wait until element is visible        accessibility_id=submit
    Tap           accessibility_id=submit
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT02/${Screenshot}02-03.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    # Wait until element is visible           chain=**/XCUIElementTypeOther[`label == " กล่องข้อความ"`]
    # Tap           chain=**/XCUIElementTypeOther[`label == " กล่องข้อความ"`]
    Sleep   3s

TCAT-03-01 Tracking Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Tracking
    Tap           accessibility_id=grid.Tracking
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-01.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s

TCAT-03-02 Tracking Add
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Tracking
    Tap           accessibility_id=grid.Tracking
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-02-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=TextButton
    Tap            accessibility_id=TextButton
    Wait until element is visible           accessibility_id=code
    Input Text        accessibility_id=code          ${Name_Tracking}
    Sleep     2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="product_type"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap            xpath=//android.widget.Button[@content-desc="product_type"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep       2s
    # Tap            xpath=//android.widget.Button[@content-desc="product_type"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText

    Tap             accessibility_id=product_type.car
    Wait until element is visible           accessibility_id=qty
    Input Text       accessibility_id=qty         2
    Wait until element is visible           accessibility_id=product_remark
    Input Text      accessibility_id=product_remark        test
    Wait until element is visible           accessibility_id=user_remark 
    Input Text      accessibility_id=user_remark        test
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-02-2.png
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Tap             accessibility_id=submit
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-02-3.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s


TCAT-03-03 Tracking Edit 


    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Tracking
    Tap           accessibility_id=grid.Tracking
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-03-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=tracking.item.${Name_Tracking}
    Tap           accessibility_id=tracking.item.${Name_Tracking}
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-03-2.png

    # Tap      accessibility_id=scrollView
    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    FOR    ${i}    IN RANGE    0    8
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END

    Sleep       2s
    Wait until element is visible           accessibility_id=navigateEdit
    Tap           accessibility_id=navigateEdit
    Sleep       2s
    # Press Key       chain=**/XCUIElementTypeOther[`label == "ประเภทสินค้า *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton       
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-03-3.png
    Sleep       3s
    Input Text       accessibility_id=qty      3
    Input Text       accessibility_id=product_remark     test1   
    Input Text       accessibility_id=user_remark        test1
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-03-4.png
    Sleep       2s
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Tap             accessibility_id=submit
    Sleep   3s

TCAT-03-04 Tracking Edit Remark
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Tracking
    Tap           accessibility_id=grid.Tracking
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-04-5.png
    Wait until element is visible           accessibility_id=tracking.item.${Name_Tracking}
    Tap           accessibility_id=tracking.item.${Name_Tracking}
    Sleep       2s

    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    FOR    ${i}    IN RANGE    0    8
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END

    Sleep       3s
    Wait until element is visible           accessibility_id=user_remark
    Input Text           accessibility_id=user_remark         2
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-04-6.png
    Sleep       2s
    FOR    ${i}    IN RANGE    0    2
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END
    Sleep       3s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-04-7.png
    Sleep       3s

TCAT-03-05 Tracking Delete
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Tracking
    Tap           accessibility_id=grid.Tracking
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-05-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=tracking.item.${Name_Tracking}
    Tap           accessibility_id=tracking.item.${Name_Tracking}
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-05-2.png
    Wait until element is visible           accessibility_id=onDelete
    Tap           accessibility_id=onDelete
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-05-3.png
    Wait until element is visible           id=android:id/button2
    Tap           id=android:id/button2
    Capture Page Screenshot          ${folder}/Capture-TCAT03/${Screenshot}03-05-4.png
    Sleep       3s

TCAT-04-01 Bill Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Bill
    Tap             accessibility_id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-01-2.png
    Wait until element is visible           accessibility_id=tab.0
    Tap             accessibility_id=tab.0
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.1
    Tap             accessibility_id=tab.1
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.2
    Tap             accessibility_id=tab.2
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.3
    Tap             accessibility_id=tab.3
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.4
    Tap             accessibility_id=tab.4
    Sleep   2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s


TCAT-04-02 Bill Add
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Bill
    Tap             accessibility_id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-2.png
    Wait until element is visible           accessibility_id=TextButton
    Tap             accessibility_id=TextButton
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-3.png
    Wait until element is visible           accessibility_id=tracking.item.${Bill_Tracking}
    Tap             accessibility_id=tracking.item.${Bill_Tracking}
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-4.png
    Sleep       3s
    FOR    ${i}    IN RANGE    0    6
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-5.png
    Sleep       3s
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-6.png
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="thai_shipping_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap             xpath=//android.widget.Button[@content-desc="thai_shipping_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=thai_shipping_id.2
    Tap             accessibility_id=thai_shipping_id.2
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="user_address_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap             xpath=//android.widget.Button[@content-desc="user_address_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=user_address_id.33369
    Tap             accessibility_id=user_address_id.33369
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-7.png
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-8.png
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="coupon_code.select"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap             xpath=//android.widget.Button[@content-desc="coupon_code.select"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=coupon_code.select.0
    Tap             accessibility_id=coupon_code.select.0
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-9.png
    FOR    ${i}    IN RANGE    0    2
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    
    Wait until element is visible           accessibility_id=user_remark
    Input Text             accessibility_id=user_remark      test
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-10.png
    # Wait until element is visible           accessibility_id=onCouponCheck
    # Tap             accessibility_id=onCouponCheck
    Sleep   2s
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-02-11.png
    Sleep   10s

    

TCAT-04-03 Bill Cancel
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-03-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Bill
    Tap             accessibility_id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-03-2.png
    Wait until element is visible           accessibility_id=bill.item.476076
    Tap             accessibility_id=bill.item.476076
    Sleep   2s
    FOR    ${i}    IN RANGE    0    8
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END  
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-03-3.png
    Sleep   3s
    Wait until element is visible           accessibility_id=onDelete
    Tap             accessibility_id=onDelete
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-03-4.png
    # Wait until element is visible           id=ยกเลิก
    # Tap           id=ยกเลิก
    Wait until element is visible           id=android:id/button2
    Tap           id=android:id/button2
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-03-5.png
    Sleep       5s


TCAT-04-04 Bill Payment
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Bill
    Tap             accessibility_id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-2.png
    Wait until element is visible           accessibility_id=tab.2
    Tap             accessibility_id=tab.2
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-3.png
    Wait until element is visible           accessibility_id=bill.item.476094
    Tap             accessibility_id=bill.item.476094
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-4.png
    Sleep       2s
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END  
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-5.png
    Wait until element is visible           accessibility_id=onPayment
    Tap             accessibility_id=onPayment
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-6.png
    # Wait until element is visible           id=ยกเลิก
    # Tap           id=ยกเลิก
    Wait until element is visible           id=android:id/button2
    Tap           id=android:id/button2
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-04-7.png
    Sleep       5s
    

TCAT-04-05 Bill Edit Remark
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex

    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-05-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Bill
    Tap             accessibility_id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-05-2.png
    Wait until element is visible           accessibility_id=bill.item.476097
    Tap             accessibility_id=bill.item.476097
    Sleep   3s
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END  
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-05-3.png
    Sleep   2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text             accessibility_id=user_remark         test
    Wait until element is visible           accessibility_id=Tracking
    Tap             accessibility_id=Tracking
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-05-4.png
    Sleep       2s
    Wait until element is visible           accessibility_id=submit
    Tap             accessibility_id=submit
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT04/${Screenshot}04-05-5.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s

TCAT-05-01 Topup Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.TopUp
    Tap             accessibility_id=grid.TopUp
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.approved
    Tap             accessibility_id=tab.approved
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-01-2.png

    Wait until element is visible            accessibility_id=topUp.item.356686
    Click element              accessibility_id=topUp.item.356686
    Sleep   3s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Wait until element is visible           accessibility_id=tab.cancel
    Tap           accessibility_id=tab.cancel
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-01-3.png
    Sleep   3s
    Wait until element is visible           	accessibility_id=topUp.item.356679
    Tap             	accessibility_id=topUp.item.356679
    Sleep   5s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s

TCAT-05-02 Topup Add

    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.TopUp
    Tap             accessibility_id=grid.TopUp
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-02-2.png
    Wait until element is visible           accessibility_id=TextButton
    Tap             accessibility_id=TextButton
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-02-3.png
    Sleep   2s
    Wait until element is visible           accessibility_id=amount
    Input Text          accessibility_id=amount            100
    Sleep   2s
    Tap             xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END  
    Tap             xpath=//android.widget.Button[@content-desc="method"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=method.transfer
    Tap             accessibility_id=method.transfer
    Wait until element is visible       xpath=//android.widget.Button[@content-desc="system_bank_account_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap             xpath=//android.widget.Button[@content-desc="system_bank_account_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep   2s
    Wait until element is visible           accessibility_id=system_bank_account_id.3201
    Tap             accessibility_id=system_bank_account_id.3201
    # FOR    ${i}    IN RANGE    0    2
    #     Swipe   0       495     0       100
    #     # ${x}=Set Variable   ${x}+1
    #     # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    # END  
    Sleep   2s
    Wait until element is visible       accessibility_id=date
    Tap             accessibility_id=date
    Sleep   2s

    Wait until element is visible       id=android:id/button1
    Tap             id=android:id/button1
    Sleep   2s
    Wait until element is visible       accessibility_id=time
    Tap             accessibility_id=time
    Wait until element is visible       id=android:id/button1
    Tap             id=android:id/button1
    Sleep   3s
    FOR    ${i}    IN RANGE    0    3
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    Wait until element is visible       accessibility_id=file.gallery
    Tap           accessibility_id=file.gallery
    Sleep       3s
    Tap           id=com.android.permissioncontroller:id/permission_allow_button
    Sleep       3s
    Tap           id=com.android.documentsui:id/icon_thumb
    Sleep      2s
    Tap             xpath=//android.view.ViewGroup[@content-desc="date"]/android.widget.TextView
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    Input Text          accessibility_id=user_remark          test
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-02-4.png
    Sleep   2s
    Wait until element is visible       accessibility_id=submit
    Tap             accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT05/${Screenshot}05-02-5.png
    Sleep       3s





TCAT-06-01 Withdraw Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Withdraw
    Tap             accessibility_id=grid.Withdraw
    Sleep   2s
    Wait until element is visible           accessibility_id=tab.approved
    Tap             accessibility_id=tab.approved
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-01-2.png
    Sleep   2s
    Wait until element is visible            accessibility_id=withdraw.item.5122
    Click element              accessibility_id=withdraw.item.5122
    Sleep   3s

    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Wait until element is visible           accessibility_id=tab.cancel
    Tap           accessibility_id=tab.cancel
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-01-3.png

    Wait until element is visible           	accessibility_id=withdraw.item.5117
    Tap             	accessibility_id=withdraw.item.5117
    Sleep   3s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s

TCAT-06-02 Withdraw Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Withdraw
    Tap             accessibility_id=grid.Withdraw
    Sleep   3s
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-02-2.png
    Sleep   2s
    Wait until element is visible           accessibility_id=TextButton
    Tap             accessibility_id=TextButton
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-02-3.png
    Sleep   2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="user_bank_account_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap                     xpath=//android.widget.Button[@content-desc="user_bank_account_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=user_bank_account_id.82
    Tap                     accessibility_id=user_bank_account_id.82
    Sleep       2s
    Input Text      accessibility_id=amount           99
    Input Text      accessibility_id=user_remark    test
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-02-4.png
    Sleep       3s
    FOR    ${i}    IN RANGE    0    5
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END   
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT06/${Screenshot}06-02-5.png
    Wait until element is visible          accessibility_id=submit
    Tap                     accessibility_id=submit
    Sleep       3s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   3s


TCAT-07-01 Statement Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT07/${Screenshot}07-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Statement
    Tap           accessibility_id=grid.Statement
    Capture Page Screenshot          ${folder}/Capture-TCAT07/${Screenshot}07-01-2.png
    Sleep       3s
    Wait until element is visible           accessibility_id=statement.item.820084
    Tap           accessibility_id=statement.item.820084

    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT07/${Screenshot}07-01-3.png
    # Wait until element is visible            accessibility_id=IconButton
    # Tap            accessibility_id=IconButton
    Sleep       5s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep   2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s

TCAT-08-01 Address Add
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=grid.Address
    Tap           accessibility_id=grid.Address
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-01-2.png
    Wait until element is visible           accessibility_id=TextButton
    Tap           accessibility_id=TextButton
    Sleep   5s
    Wait until element is visible           accessibility_id=name         
    Input Text              accessibility_id=name            MOBILE_ADD2
    Sleep         2s
    Wait until element is visible           accessibility_id=tel
    Input Text              accessibility_id=tel              0934567891
    Sleep         2s
    Wait until element is visible           accessibility_id=address
    Input Text            accessibility_id=address         13/34 ttest road, 
    Sleep         2s
    Wait until element is visible            xpath=//android.widget.Button[@content-desc="province_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="province_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s

    Wait until element is visible           accessibility_id=province_id.1
    Tap           accessibility_id=province_id.1
    Sleep         3s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="amphur_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="amphur_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s
    Wait until element is visible           accessibility_id=amphur_id.33
    Tap           accessibility_id=amphur_id.33    
    Sleep         3s
    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END 
    Sleep         3s
    Wait until element is visible            xpath=//android.widget.Button[@content-desc="district_code"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="district_code"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s
    Wait until element is visible           accessibility_id=district_code.103302
    Tap           accessibility_id=district_code.103302
    Sleep         3s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="status"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="status"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text              accessibility_id=user_remark              test
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-01-3.png
    Sleep         2s
    Tap        accessibility_id=status
    Sleep         2s
    Tap        accessibility_id=status.active
    Sleep         2s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-01-4.png
    Sleep       3s


TCAT-08-02 Address Edit
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Address
    Tap           accessibility_id=grid.Address
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-02-2.png
    Wait until element is visible           accessibility_id=address.item.75143
    Tap           accessibility_id=address.item.75143
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-02-3.png
    Sleep       2s

    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END 
    Sleep         4s
    # Wait until element is visible           accessibility_id=status
    # Tap           accessibility_id=status
    # Sleep         3s
    # Wait until element is visible           accessibility_id=status.inactive
    # Tap           accessibility_id=status.inactive
    # Sleep         2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text              accessibility_id=user_remark              22
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-02-4.png
    Sleep         2s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT08/${Screenshot}08-02-5.png
    Sleep       3s


TCAT-09-01 NoCode Search
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element             accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT09/${Screenshot}09-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.NoCode
    Tap           accessibility_id=grid.NoCode
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT09/${Screenshot}09-01-2.png
    Wait until element is visible           accessibility_id=IconButton
    Tap             accessibility_id=IconButton
    Capture Page Screenshot          ${folder}/Capture-TCAT09/${Screenshot}09-01-3.png
    Sleep   2s
    Wait until element is visible          accessibility_id=type
    Tap           accessibility_id=type
    Sleep       3s
    Wait until element is visible           accessibility_id=type.bag
    Tap                 accessibility_id=type.bag
    Capture Page Screenshot          ${folder}/Capture-TCAT09/${Screenshot}09-01-4.png
    Sleep   2s
    Wait until element is visible           accessibility_id=submit
    Tap                 accessibility_id=submit
    Sleep       5s
    Capture Page Screenshot          ${folder}/Capture-TCAT09/${Screenshot}09-01-5.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-10-01 Contact Display
    Wait until element is visible           accessibility_id=HomeIndex
    Click Element            accessibility_id=HomeIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT10/${Screenshot}10-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=grid.Contact
    Tap           accessibility_id=grid.Contact
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT10/${Screenshot}10-01-2.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s
    

TCAT-11-01 News and Promotion Display
    Sleep   3s
    Wait until element is visible          accessibility_id=NewsIndex
    Click Element             accessibility_id=NewsIndex
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT11/${Screenshot}11-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=news.items.1758
    Tap             accessibility_id=news.items.1758
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT11/${Screenshot}11-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-12-01 Account Edit
    Wait until element is visible            accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT12/${Screenshot}12-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=info
    Tap           accessibility_id=info
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT12/${Screenshot}12-01-2.png
    Wait until element is visible           accessibility_id=birthday
    Tap           accessibility_id=birthday
    Sleep       2s
    Wait until element is visible           id=android:id/button1
    Tap           id=android:id/button1
    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END 
    Sleep       2s
    # Wait until element is visible           accessibility_id=sms_notification
    # Tap           accessibility_id=sms_notification
    # Sleep       2s
    # Wait until element is visible           accessibility_id=sms_notification.inactive
    # Tap           accessibility_id=sms_notification.inactive
    # Sleep       2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text              accessibility_id=user_remark              1
    Capture Page Screenshot          ${folder}/Capture-TCAT12/${Screenshot}12-01-3.png
    Sleep       2s
    Wait until element is visible           accessibility_id=submit
    Tap            accessibility_id=submit
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Capture Page Screenshot          ${folder}/Capture-TCAT12/${Screenshot}12-01-4.png
    Sleep       3s






TCAT-13-01 Address Add
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=location-pin
    Tap           accessibility_id=location-pin
    Sleep       3s
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-01-2.png
    Wait until element is visible           accessibility_id=TextButton
    Tap           accessibility_id=TextButton
    Sleep   5s
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-01-3.png
    Wait until element is visible           accessibility_id=name         
    Input Text              accessibility_id=name            MOBILE_ADD3
    Wait until element is visible           accessibility_id=tel
    Input Text              accessibility_id=tel              0934567891
    Sleep         2s
    Wait until element is visible           accessibility_id=address
    Input Text            accessibility_id=address         13/34 ttest road, 
    Sleep         2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="province_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="province_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s
    Wait until element is visible           accessibility_id=province_id.1
    Tap           accessibility_id=province_id.1
    Sleep         3s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="amphur_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="amphur_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep         2s
    Wait until element is visible           accessibility_id=amphur_id.33
    Tap           accessibility_id=amphur_id.33    
    Sleep         3s
    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END 
    Sleep         3s
    Wait until element is visible            accessibility_id=district_code
    Tap           accessibility_id=district_code
    Sleep         2s
    Tap           accessibility_id=district_code

    Wait until element is visible           accessibility_id=district_code.103303
    Tap           accessibility_id=district_code.103303
    Sleep         3s
    Wait until element is visible       accessibility_id=status    
    Tap        accessibility_id=status
    Sleep         2s
    Tap        accessibility_id=status.active
    Sleep         2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text              accessibility_id=user_remark              test
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-01-4.png
    Sleep         2s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-01-5.png
    Sleep       3s

TCAT-13-02 Address Edit
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-02-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=location-pin
    Tap           accessibility_id=location-pin
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-02-2.png
    Sleep       3s
    Wait until element is visible           accessibility_id=address.item.75143
    Tap           accessibility_id=address.item.75143
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-02-3.png
    Sleep       2s
    Tap         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    7
        Swipe   0       495     0       100
        # ${x}=Set Variable   ${x}+1
        # Scroll Down         xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]
    END 
    Sleep         4s
    Wait until element is visible           accessibility_id=user_remark
    Input Text              accessibility_id=user_remark              2
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-02-4.png
    Sleep       2s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT13/${Screenshot}13-02-5.png
    Sleep       3s


TCAT-14-01 Bank Add
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=bank
    Tap           accessibility_id=bank
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=TextButton
    Tap           accessibility_id=TextButton
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-01-3.png
    Sleep       2s
    Wait until element is visible           	accessibility_id=bank
    Tap          accessibility_id=bank
    Sleep       2s
    Wait until element is visible           accessibility_id=bank.SCB
    Tap            accessibility_id=bank.SCB
    Sleep       2s
    Wait until element is visible           accessibility_id=branch
    Input Text      accessibility_id=branch           SRIRACHA
    Sleep       2s
    Wait until element is visible           accessibility_id=title
    Input Text      accessibility_id=title                MOBILE_TEST
    Sleep       2s
    Wait until element is visible           accessibility_id=account
    Input Text      accessibility_id=account          7583838923
    Tap          xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    2
        Swipe   0       495     0       100
    END 
    Sleep       2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="status"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep       2s
    Click Element           xpath=//android.widget.Button[@content-desc="status"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep       2s
    Click Element           xpath=//android.widget.Button[@content-desc="status"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Wait until element is visible           accessibility_id=status.active
    Tap           accessibility_id=status.active
    Wait until element is visible           accessibility_id=user_remark
    Input Text      accessibility_id=user_remark          test
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-01-4.png
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-01-5.png
    Sleep       3s



TCAT-14-02 Bank Edit
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-02-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=bank
    Tap           accessibility_id=bank
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-02-2.png
    Wait until element is visible           accessibility_id=bank.item.7975
    Tap           accessibility_id=bank.item.7975
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-02-3.png
    Wait until element is visible           accessibility_id=title
    Input Text      accessibility_id=title            125667667
    Tap          xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    2
        Swipe   0       495     0       100
    END 
    Sleep       2s
    Wait until element is visible           accessibility_id=user_remark
    Input Text      accessibility_id=user_remark          qqee
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-02-4.png
    Sleep       2s
    Tap          xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT14/${Screenshot}14-02-5.png
    Sleep       3s


TCAT-15-01 PointOrder Display
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=gift
    Tap           accessibility_id=gift
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-01-2.png
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-15-02 PointOrder Add
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=gift
    Tap           accessibility_id=gift
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-2.png
    Sleep   2s
    Wait until element is visible           accessibility_id=TextButton
    Tap           accessibility_id=TextButton
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-3.png
    Sleep   2s
    Wait until element is visible           accessibility_id=onIncrease
    Tap           accessibility_id=onIncrease
    Sleep   2s
    Wait until element is visible           xpath=//android.widget.ScrollView[@content-desc="reward.FlatList"]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.Button
    Tap           xpath=//android.widget.ScrollView[@content-desc="reward.FlatList"]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.Button
    Sleep   2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-4.png
    Sleep       2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="shipping_method_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="shipping_method_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep       2s
    Wait until element is visible          accessibility_id=shipping_method_id.2
    Tap           accessibility_id=shipping_method_id.2
    Sleep       2s
    Wait until element is visible           xpath=//android.widget.Button[@content-desc="address_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Tap           xpath=//android.widget.Button[@content-desc="address_id"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText
    Sleep       3s
    Wait until element is visible          accessibility_id=address_id.33483
    Tap           accessibility_id=address_id.33483
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT15/${Screenshot}15-02-5.png
    Sleep       3s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-6.png
    Sleep       3s
    Input Text      accessibility_id=user_remark          test

    # Tap           xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup/android.view.ViewGroup[4]/android.view.ViewGroup
    # FOR    ${i}    IN RANGE    0    2
    #     Swipe   0       495     0       100
    # END 
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-7.png
    Sleep       2s
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-02-8.png
    Sleep       3s



TCAT-15-03 PointOrder Edit Remark
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-03-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=gift
    Tap           accessibility_id=gift
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-03-2.png
    Wait until element is visible           accessibility_id=pointOrder.item.1333
    Tap           accessibility_id=pointOrder.item.1333
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-03-3.png
    Sleep       2s
    FOR    ${i}    IN RANGE    0    6
        Swipe   0       495     0       100
    END 
    Sleep       2s
    Input Text       accessibility_id=user_remark          test
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-03-4.png
    Wait until element is visible           accessibility_id=submit
    Tap           accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-03-5.png
    Sleep       3s    


TCAT-15-04 PointOrder Cancel

    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-04-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=gift
    Tap           accessibility_id=gift
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-04-2.png
    Wait until element is visible           accessibility_id=pointOrder.item.1349
    Tap           accessibility_id=pointOrder.item.1349
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-04-3.png
    Wait until element is visible           	xpath=//android.widget.Button[@content-desc="onDelete"]/android.widget.TextView
    Tap           xpath=//android.widget.Button[@content-desc="onDelete"]/android.widget.TextView
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-04-4.png
    Wait until element is visible           id=android:id/button2
    # Tap           chain=**/XCUIElementTypeButton[`label == "ยกเลิก"`]  
    Tap           id=android:id/button2
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-04-5.png
    Sleep       3s  
# **/XCUIElementTypeButton[`label == "ตกลง"`]


TCAT-15-05 PointOrder Payment
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-05-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=gift
    Tap           accessibility_id=gift
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-05-2.png
    Wait until element is visible           accessibility_id=pointOrder.item.1351
    Tap           accessibility_id=pointOrder.item.1351
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-05-3.png
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
    END 
    Sleep       2s
    Wait until element is visible           accessibility_id=onPayment
    Tap           accessibility_id=onPayment
    Capture Page Screenshot          ${folder}/Capture-TCAT15/${Screenshot}15-05-4.png
    Wait until element is visible           id=android:id/button2
    # Tap           id=android:id/button1
    Tap           id=android:id/button2
    Capture Page Screenshot         ${folder}/Capture-TCAT15/${Screenshot}15-05-5.png
    Sleep       3s  






TCAT-22-01 Coupon Add

    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex

    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-01-2.png
    Wait until element is visible           accessibility_id=TextButton
    Tap           accessibility_id=TextButton
    Sleep       2s
    Click Element           xpath=(//android.widget.Button[@content-desc="onIncrease"])[1]
    Sleep       2s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-01-3.png
    Wait until element is visible           xpath=//android.widget.ScrollView[@content-desc="billCoupon.FlatList"]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.Button
    Tap           xpath=//android.widget.ScrollView[@content-desc="billCoupon.FlatList"]/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.Button
    Sleep       3s 
    Wait until element is visible           accessibility_id=user_remark
    Input Text           accessibility_id=user_remark            Test
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-01-4.png
    Wait until element is visible           accessibility_id=submit
    Tap         accessibility_id=submit
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-01-5.png
    Sleep       3s 



TCAT-22-02 Coupon Edit Remark
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-02-2.png
    Wait until element is visible           accessibility_id=billCoupon.item.1073
    Tap                 accessibility_id=billCoupon.item.1073
    # Click Element           xpath=//android.widget.ScrollView[@content-desc="scrollView"]/android.view.ViewGroup
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
    END 
    Wait until element is visible           accessibility_id=user_remark
    Input Text        accessibility_id=user_remark         TEST
    Sleep     3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-02-3.png
    Sleep       3s
    Wait until element is visible           accessibility_id=submit
    Click Element       accessibility_id=submit
    Sleep     3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-02-4.png
    Sleep     3s
แ



TCAT-22-03 Coupon Payment
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-03-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-03-2.png
    Wait until element is visible           accessibility_id=billCoupon.item.1074
    Tap                 accessibility_id=billCoupon.item.1074
    FOR    ${i}    IN RANGE    0    4
        Swipe   0       495     0       100
    END 
    Wait until element is visible           accessibility_id=onPayment
    Tap        accessibility_id=onPayment
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-03-3.png
    Sleep       3s
    Wait until element is visible           id=android:id/button1
    Click Element        id=android:id/button1
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-03-4.png
    Sleep       3s


TCAT-22-04 Coupon Delete
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-04-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-04-2.png
    Wait until element is visible           accessibility_id=billCoupon.item.1075
    Tap                 accessibility_id=billCoupon.item.1075
    Sleep       3s
    Wait until element is visible           accessibility_id=onDelete
    Click Element                  accessibility_id=onDelete
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-04-3.png
    Sleep       2s
    Wait until element is visible           id=android:id/button1
    Tap        id=android:id/button1
    Sleep       2s
    Capture Page Screenshot           ${folder}/Capture-TCAT22/${Screenshot}22-04-4.png
    Sleep       3s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-23-01 Coupon free Display

    Wait until element is visible           accessibility_id=AccountIndex
    Click Element           accessibility_id=AccountIndex
    Sleep       3s
    Capture Page Screenshot           ${folder}/Capture-TCAT23/${Screenshot}23-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket
    Tap            accessibility_id=ticket
    Sleep       3s
    Capture Page Screenshot           ${folder}Capture-TCAT23/${Screenshot}23-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=coupon.item.${namecouponfree}
    Tap           accessibility_id=coupon.item.${namecouponfree}
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT23/${Screenshot}23-01-2.png
    Sleep       3s








TCAT-16-01 Coupon Display
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT16/${Screenshot}16-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation-outline
    Tap           accessibility_id=ticket-confirmation-outline
    Capture Page Screenshot          ${folder}/Capture-TCAT16/${Screenshot}16-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=tab.availables
    Tap           accessibility_id=tab.availables
    Sleep       2s
    Wait until element is visible           accessibility_id=tab.useds
    Tap           accessibility_id=tab.useds
    Sleep       2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s



TCAT-17-01 Change Password 
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT17/${Screenshot}17-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=key
    Tap           accessibility_id=key
    Sleep       2s
    Wait until element is visible           accessibility_id=password_current
    Input Text          accessibility_id=password_current             123456
    Wait until element is visible           accessibility_id=password
    Input Text          accessibility_id=password               123456
    Wait until element is visible           accessibility_id=password_confirmation
    Input Text          accessibility_id=password_confirmation            123456
    Capture Page Screenshot          ${folder}/Capture-TCAT17/${Screenshot}17-01-2.png
    Sleep       2s
    Tap         accessibility_id=submit
    Capture Page Screenshot          ${folder}/Capture-TCAT17/${Screenshot}17-01-3.png
    Sleep       3s
    



TCAT-18-01 Contact Display
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT18/${Screenshot}18-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=bubbles
    Tap           accessibility_id=bubbles
    Capture Page Screenshot          ${folder}/Capture-TCAT18/${Screenshot}18-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-19-01 Info Display
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT19/${Screenshot}19-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=wallet
    Tap           accessibility_id=wallet
    Capture Page Screenshot          ${folder}/Capture-TCAT19/${Screenshot}19-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s

TCAT-20-01 Terms of Service 
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT20/${Screenshot}20-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=pin
    Tap           accessibility_id=pin
    Capture Page Screenshot          ${folder}/Capture-TCAT20/${Screenshot}20-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=backButton
    Tap            accessibility_id=backButton
    Sleep       3s


TCAT-21-01 Log out
    Wait until element is visible           accessibility_id=AccountIndex
    Click Element            accessibility_id=AccountIndex
    Capture Page Screenshot          ${folder}/Capture-TCAT21/${Screenshot}21-01-1.png
    Sleep       2s
    Wait until element is visible           accessibility_id=log-out
    Tap           accessibility_id=log-out
    Sleep       2s
    Capture Page Screenshot          ${folder}/Capture-TCAT21/${Screenshot}21-01-2.png
    Wait until element is visible           id=android:id/button2
    Click Element           id=android:id/button2
    Sleep       3s
    Capture Page Screenshot         ${folder}/Capture-TCAT21/${Screenshot}21-01-3.png
    Sleep       3s
